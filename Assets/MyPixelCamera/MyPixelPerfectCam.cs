﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode()]
public class MyPixelPerfectCam : MonoBehaviour {
    public bool fitByWidth = true;
    public bool fitByHeight = false;
    public int baseWidth = 300;
    public int baseHeight = 200;
    public MeshRenderer meshRenderer;
    Camera cam;
    Camera renderCamera;
    int textureWidth;
    int textureHeight;

	void Start () {
        cam = GetComponent<Camera>();
        renderCamera = transform.GetChild(0).GetComponent<Camera>();
        if (fitByWidth)
        {
            textureWidth = baseWidth;
            textureHeight = (int)(baseWidth* (float)Screen.height/(float)Screen.width);
        }
        if (fitByHeight)
        {
            textureHeight = baseHeight;
            textureWidth = (int)(baseHeight * (float)Screen.width / (float)Screen.height);
        }
        RenderTexture texture = GenerateTexture();  
        cam.targetTexture = texture;
        meshRenderer.sharedMaterial.mainTexture = texture;
        ScaleBehaviour();
	}

    public RenderTexture GenerateTexture()
    {
        RenderTexture texture = new RenderTexture(textureWidth, textureHeight, 24, RenderTextureFormat.ARGB32);
        texture.filterMode = FilterMode.Point;
        texture.DiscardContents();
        return texture;
    }

    private void ScaleBehaviour()
    {
        float windowAspectRatio = Screen.width / (float)Screen.height;
        meshRenderer.transform.localScale = new Vector3(windowAspectRatio*10, 10f, 10f);
      
    }
}
