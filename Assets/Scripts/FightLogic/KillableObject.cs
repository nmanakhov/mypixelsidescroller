﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillableObject : MonoBehaviour {
    public string name;
    public int currentHealth;
    public delegate void MethodContainer();
    public event MethodContainer onHealthChanged;

    public virtual void TakeDamage(int delta)
    {
        currentHealth -= delta;
        onHealthChanged();
        if (currentHealth <= 0)
        {
            GameObject temp = new GameObject();

            PixelizatorMesh pixelizatorMesh = temp.AddComponent<PixelizatorMesh>();
            pixelizatorMesh.Init();
            SpriteRenderer spriteRenderer = GetComponentInChildren<SpriteRenderer>();
            temp.transform.position = spriteRenderer.transform.position;
            Debug.Log(spriteRenderer.transform.position);
            pixelizatorMesh.testSprite = spriteRenderer.sprite;
            pixelizatorMesh.pixCountWidth = pixelizatorMesh.pixCountHeight = 4;
            pixelizatorMesh.GenerateMesh(pixelizatorMesh.testSprite, spriteRenderer.color);
            Destroy(this.gameObject);
        }
    }
}
