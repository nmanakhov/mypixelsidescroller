﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : ScriptableObject {
    public bool isWeaponAttack = true;
    public Sprite sprite;
    public int coolDown;
    public string description;
    protected CreatureController creatureController;
    protected CreatureController target;
    protected CustomAnimator animator;
    protected PlayerInputHandler playerInputHandler;
    public virtual void Init(CreatureController creatureController, CreatureController target)
    {
        this.creatureController = creatureController;
        this.animator = creatureController.GetComponent<CustomAnimator>();
        playerInputHandler = creatureController.GetComponent<PlayerInputHandler>();
        this.target = target;
    }

    public virtual void Use()
    {

    }

    public virtual string GetDescription()
    {
        return "Oops";
    }
}
