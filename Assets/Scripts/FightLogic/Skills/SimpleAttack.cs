﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SimpleAttack", menuName = "ScriptableObjects/SimpleAttack")]
public class SimpleAttack : Skill {
    int currentState = 0;
    public override void Init(CreatureController creatureController, CreatureController target)
    {
        base.Init(creatureController, target);
        currentState = 0;
    }
    public override void Use()
    {
        if (!playerInputHandler.IsFight && !playerInputHandler.IsMoving)
        {
            creatureController.PopState();
            return;
        }

        switch (currentState) {
            case 0:
                this.animator.SetAnimation("Attack0");
                if (this.animator.currentFrame == this.animator.GetCurrentAnimationLength() - 1)
                {
                    creatureController.PopState();
                    if (playerInputHandler.IsMoving || playerInputHandler.IsFight)
                    creatureController.GetComponent<PlayerInputHandler>().ChangeActionPoint(-1);
                    if (target != null && Mathf.Abs((target.transform.position-this.creatureController.transform.position).x)<40)
                        target.GetComponent<Creature>().TakeDamage(5);
                    currentState = 1;
                    this.animator.CurrentFrame = 0;
                }
                break;
            case 1:

            break;
        }
    }
}
