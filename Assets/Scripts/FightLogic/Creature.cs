﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : KillableObject {
    [Header("Basic parameters")]
    public float strength;
    public float agility;
    public float intelligence;
    [Header("Damages")]
    public Damage[] damages;
    [Header("Resists")]
    public Resist[] resistance;
    public float maxHealth;
    public int experience;
    public int level;
    public event MethodContainer onExperienceChanged;
    public event MethodContainer onLevelUp;
    public virtual void TakeDamage(float delta, Damage[] damages)
    {
        float totalDamage = 0f;
        for (int i = 0; i < damages.Length; i++)
        {
            for (int j = 0; j < resistance.Length; j++)
            {
                if (damages[i].damageType == resistance[j].damageType)
                    totalDamage = damages[i].value * (1f - resistance[j].value);
            }
        }

        base.TakeDamage((int)totalDamage);
    }

    public virtual void TakeExperience(int exp)
    {
        experience += exp;
        onExperienceChanged();
        if (experience >= GameManager.experienceTable[level])
            LevelUp();
    }

    public virtual void LevelUp()
    {
        TakeExperience(-GameManager.experienceTable[level]);
        onLevelUp();
    }
}
