﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageTypes {
    Air, Earth, Fire, Water, Poison, Bleed, Stabbing, Crushing
}
