﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightController : MonoBehaviour {
    Queue<PlayerInputHandler> creatures;
    KnightController knightController;
    Room fightingRoom;

    public delegate void MethodContainer();
    MethodContainer currentState;
    CameraFollow cameraFollow;
    public void Start()
    {
        currentState = PeaceTime;
        cameraFollow = GameManager.Instance.cameraFollow;
    }

    public void StartFight()
    {
        knightController = KnightController.Instance;
        fightingRoom = knightController.currentRoom;
        if (fightingRoom.creatures != null)
        {
            currentCreature = null;
            creatures = new Queue<PlayerInputHandler>();
            knightController.currentRoom.CloseDoors();
            PlayerInputHandler playerInputHandler = knightController.GetComponent<PlayerInputHandler>();
            playerInputHandler.IsMoving = false;
            playerInputHandler.SetActionPoint(5);
            creatures.Enqueue(playerInputHandler);
            for (int i = 0; i < fightingRoom.creatures.Count; i++)
            {
                PlayerInputHandler pih = fightingRoom.creatures[i].GetComponent<PlayerInputHandler>();
                pih.SetActionPoint(5);
                pih.IsMoving = false;
                creatures.Enqueue(pih);
            }
            currentState = FightTime;
            GameManager.Instance.fightSystemUI.SetButtons(false);
        }
        
    }

    public void EndFight()
    {

    }

    public PlayerInputHandler currentCreature;
    public void Update()
    {
        currentState();
    }

    public void FightTime()
    {
        
        if (currentCreature == null)
        {
            currentCreature = creatures.Dequeue();
            cameraFollow.creature = currentCreature.transform;
            currentCreature.IsFight = true;
        }

        if (currentCreature.IsDead() || currentCreature.actionPoints == 0 || !currentCreature.IsFight)
        {
            currentCreature.ChangeActionPoint(5);
            if (!currentCreature.IsDead())
                creatures.Enqueue(currentCreature);
            currentCreature.IsFight = false;
            currentCreature = null;
            cameraFollow.creature = null;
        }

        if (creatures.Count == 0)
        {
            currentCreature.IsMoving = true;
            currentCreature.IsFight = false;
            fightingRoom.OpenDoors();
            cameraFollow.creature = null;
            currentState = PeaceTime;
        }
        else

        if (creatures.Peek().IsDead() || currentCreature == creatures.Peek())
            creatures.Dequeue();
    }

    public void PeaceTime()
    {

    }

    public CreatureController GetEnemy(CreatureController controller)
    {
        if (creatures == null)
            return null;
        foreach (PlayerInputHandler cc in creatures)
            if (controller.gameObject != cc.gameObject)
                return cc.GetComponent<CreatureController>();
        return null;
    }
}
