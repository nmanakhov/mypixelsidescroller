﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class Damage {
    public float value;
    public DamageTypes damageType;
}
