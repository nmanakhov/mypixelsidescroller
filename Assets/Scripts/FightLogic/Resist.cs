﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Resist {
    public float value;
    public DamageTypes damageType;
}
