﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class CustomAnimation {
    public float deltaTime;
    public string name;
    public AnimationFrame[] frames;
}
