﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomAnimator : MonoBehaviour {
    public CustomAnimation[] animations;
    public int currentAnimation = 0;
    public int currentFrame;
    public int CurrentFrame {
        set {
            currentFrame = value;
            SetAnimationFrame();
        }

        get {
            return currentFrame;
        }
    }

    public float timer;
    Dictionary<string, int> names = new Dictionary<string, int>();
	void Start () {
        timer = animations[currentAnimation].deltaTime;
        for (int i = 0; i < animations.Length; i++)
            names.Add(animations[i].name, i);
        SetAnimationFrame();
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer < 0f)
        {
            timer += animations[currentAnimation].deltaTime;
            currentFrame++;
            if (currentFrame >= animations[currentAnimation].frames.Length)
                currentFrame = 0;
            SetAnimationFrame();
        }
	}

    public void SetAnimationFrame()
    {
                AnimationFrame animationFrame = animations[currentAnimation].frames[currentFrame];
                int keyFramesCount = animationFrame.keyFrames.Length;
                for (int ii = 0; ii<keyFramesCount; ii++) {
                    animationFrame.keyFrames[ii].ApplyFrame();
                }
    }

    public void SetAnimation(int num)
    {
        if (currentAnimation == num)
            return;
        currentAnimation = num;
        CurrentFrame = 0;
        SetAnimationFrame();
    }

    public void SetAnimation(string name)
    {
        if (names.ContainsKey(name))
        SetAnimation(names[name]);
    }

    public int GetCurrentAnimationLength()
    {
        return animations[currentAnimation].frames.Length;
    }
}
