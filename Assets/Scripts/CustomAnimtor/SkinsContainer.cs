﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class SkinsContainer : MonoBehaviour {
    public SpriteChanger spriteChanger;
    public SkinFrames[] skinFrames;
    [Serializable]
    public class SkinFrames {
        public Sprite[] frames;
    }

    public void setSkin(int i)
    {
        spriteChanger.sprites = skinFrames[i].frames;
    }
}
