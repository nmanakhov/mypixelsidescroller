﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AnimationFrame{
    public KeyFrame[] keyFrames;
    [Serializable]
    public class KeyFrame {
        public SpriteChanger spriteChanger;
        public Transform t;
        public int spriteId;
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
        public PixelRotation pixelRotation;
        public void ApplyFrame()
        {
            if (spriteChanger!=null)
            spriteChanger.CurrentFrame = spriteId;
            if (t == null)
                t = spriteChanger.transform;
            t.localPosition = position;
            if (pixelRotation == null)
                t.localRotation = Quaternion.Euler(rotation);
            else
                pixelRotation.Angle = (int)rotation.z;
            t.localScale = scale;
        }
    }
}
