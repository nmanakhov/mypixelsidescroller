﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {
    public Sprite[] sprites;
    private int currentFrame = 0;
    SpriteRenderer sr;
    public int CurrentFrame {
        set {
            if (sr == null)
                sr = GetComponent<SpriteRenderer>();
            currentFrame = value;
            if (currentFrame>-1 && currentFrame<sprites.Length && sprites[currentFrame]!=null)
                sr.sprite = sprites[currentFrame];
        }
        get {
            return currentFrame;
        }
    }
}
