﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour {
    private static RoomGenerator instance_;
    public static RoomGenerator Instance {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<RoomGenerator>();
            return instance_;
        }        
    }

    public int stageWidth = 20;
    public int stairsNum = 2;
    public int stageNum = 5;

    public GameObject roomObjects;
    Transform floors;
    Transform walls;
    Transform windows;
    Transform shadows;
    Transform creatures;
    GameObject door;
    GameObject ladder;
    GameObject tower;
    GameObject[] allWalls;
    GameObject[] allFloors;
    GameObject[] allShadows;
    GameObject[] allWindows;
    GameObject[] allCreatures;
    public List<Stage> stages;
    void Start()
    {        
       // roomObjects = GameObject.Find("RoomObjects");
        floors = roomObjects.transform.Find("floors");
        walls = roomObjects.transform.Find("walls");
        windows = roomObjects.transform.Find("windows");
        shadows = roomObjects.transform.Find("shadows");
        door = roomObjects.transform.Find("door").gameObject;
        ladder = roomObjects.transform.Find("ladder").gameObject;
        creatures = roomObjects.transform.Find("creatures");
        allWalls = GetChilds(walls);
        allFloors = GetChilds(floors);
        allShadows = GetChilds(shadows);
        allWindows = GetChilds(windows);
        allCreatures = GetChilds(creatures);
        int width = UnityEngine.Random.Range(6, 16);
        //roomObjects.SetActive(false);
        GenerateTower();
    }

    void GenerateTower()
    {
        stages = new List<Stage>();
        GameObject tower = new GameObject("Tower");
        for (int i = 0; i < stageNum; i++)
        {            
            Stage stage = GenerateStage(i);
            stage.transform.SetParent(tower.transform);
            stages.Add(stage);
        }
    }

    Stage GenerateStage(int stageId)
    {
        GameObject go = new GameObject("Stage"+stageId);
        Stage stage = go.AddComponent<Stage>();
        stage.transform.localPosition = new Vector3(0f, stageId * 5f * 32f, 0f);
        for (int i = 0; i <= stairsNum; i++)
        {
            GameObject stairs = Instantiate(ladder, go.transform);
            stairs.transform.localPosition = new Vector3(i*32*stageWidth/stairsNum, 0f, 0f);
            stage.rooms.Add(stairs.GetComponent<Room>());
        }
        int roomId = 0;
        for (int i = 1; i <= stairsNum; i++)
        {
            int spaceWidth = stageWidth / (stairsNum) - 5;
            while (spaceWidth > 5)
            {
                int roomWidth = UnityEngine.Random.Range(8, 16);
                if (spaceWidth-5 <= 16)
                    roomWidth = spaceWidth - 5;
                if (roomWidth < 5)
                    Debug.Log("error");
                spaceWidth -= roomWidth;
                Room newRoom = GenerateRoom(roomId, roomWidth);
                newRoom.transform.SetParent(stage.transform);
                newRoom.transform.localPosition = new Vector3((i*stageWidth / (stairsNum) - spaceWidth - roomWidth/2f)*32, 0f, 0f);
                stage.rooms.Add(newRoom);
                roomId++;
            }
        }
        return stage;
    }

    Room GenerateRoom(int roomId, int width)
    {
        GameObject newRoom = new GameObject("Room" + roomId);
        GameObject Walls = new GameObject("Walls");
        GameObject Floors = new GameObject("Floors");
        GameObject Shadows = new GameObject("Shadow");

        Room room = newRoom.AddComponent<Room>();
        BoxCollider2D collider = newRoom.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;
        collider.size = new Vector2(width*32f, 32f*3);
        room.tag = "Room";
        room.width = width;
        Walls.transform.SetParent(newRoom.transform);
        Floors.transform.SetParent(newRoom.transform);
        Shadows.transform.SetParent(newRoom.transform);
        Walls.transform.position = Floors.transform.position = Shadows.transform.position = Vector3.zero;
        Walls.transform.localScale = Floors.transform.localScale = Shadows.transform.localScale = Vector3.one;
        for (float i = 0; i < width; i++)
            for (float j = 0; j < 3; j++)
            {
                GameObject wallTile = Instantiate(allWalls[UnityEngine.Random.Range(0, allWalls.Length)], Walls.transform);
                wallTile.transform.localPosition = new Vector3(32f * (i - width / 2f), 32 * j, 0f);
                wallTile.transform.localScale = Vector3.one;
            }

        for (float i = 0; i < width; i++)
        {
            GameObject floorTile = Instantiate(allFloors[UnityEngine.Random.Range(0, allFloors.Length)], Floors.transform);
            floorTile.transform.localPosition = new Vector3(32f * (i - width / 2f), -32, 0f);
            floorTile.transform.localScale = Vector3.one;
        }

        for (float i = 0; i < width; i++)
        {
            GameObject floorTile = Instantiate(allShadows[UnityEngine.Random.Range(0, allShadows.Length)], Shadows.transform);
            floorTile.transform.localPosition = new Vector3(32f * (i - width / 2f), 0f, 0f);
            floorTile.transform.localScale = Vector3.one;
        }

        GameObject doorL = Instantiate(door, newRoom.transform);
        doorL.transform.localPosition = new Vector3(32f * (width / 2f - 1f) + 16f, 64, 0f);
        doorL.transform.localScale = Vector3.one;
        room.doorL = doorL.transform.GetChild(0);

        GameObject doorR = Instantiate(door, newRoom.transform);
        doorR.transform.localPosition = new Vector3(-32f * width / 2f - 16f, 64, 0f);
        doorR.transform.localScale = Vector3.one;
        room.doorR = doorR.transform.GetChild(0);

        for (int i = 0; i <= (width-1) / 3f; i++)
        {
            GameObject windowSprite = Instantiate(allWindows[UnityEngine.Random.Range(0, allWindows.Length)], Walls.transform);
            windowSprite.transform.localPosition = new Vector3((i * 3f *32f - (width)*16f), 16, 0f);
            windowSprite.transform.localScale = Vector3.one;
        }

        GameObject creature = Instantiate(allCreatures[UnityEngine.Random.Range(0, allCreatures.Length)], newRoom.transform);
        CreatureController creatureController = creature.GetComponent<CreatureController>();
        if (room.creatures == null)
            room.creatures = new List<CreatureController>();
        Morphling morphling = creature.GetComponent<Morphling>();
        if (morphling)
            morphling.Morph();

        creature.transform.localPosition = Vector3.zero + creatureController.offset;
        room.creatures.Add(creatureController);
        return room;
    }

    private static GameObject[] GetChilds(Transform t)
    {
        GameObject[] childs = new GameObject[t.childCount];
        for (int i = 0; i < t.childCount; i++)
            childs[i] = t.GetChild(i).gameObject;
        return childs;
    }

    
}
