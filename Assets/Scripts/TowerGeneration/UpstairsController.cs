﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpstairsController : MonoBehaviour {
    public GameObject upButton;
    public GameObject downButton;
    Vector3 zero = Vector3.zero;
    Vector3 one = Vector3.one;
    UpstairsUI upstairsUI;
    Material material;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (upstairsUI == null)
            upstairsUI = GameManager.Instance.upstairsUI;
        upstairsUI.ShowButtons();
        SetOutlineValue(1f);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        upstairsUI.HideButtons();
        SetOutlineValue(0f);
    }

    void SetOutlineValue(float value)
    {
         if (material == null)
            material = GetComponent<SpriteRenderer>().material;
        material.SetFloat("_Outline", value);
    }
}
