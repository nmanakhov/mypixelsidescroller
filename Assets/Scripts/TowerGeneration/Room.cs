﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {
    public Transform doorL;
    public Transform doorR;
    public int width = 5;
    public List<CreatureController> creatures; 

    public void OpenDoors()
    {
        StartCoroutine(OpenDoorsCoroutine());
    }

    public void CloseDoors()
    {
        StartCoroutine(CloseDoorsCoroutine());
    }

    Vector3 closed = new Vector3(1f, 4f, 1f);
    Vector3 open = new Vector3(1f, 0f, 1f);
    IEnumerator CloseDoorsCoroutine()
    {
        while (doorL.transform.localScale.y != 4)
        {
            doorR.transform.localScale = doorL.transform.localScale = Vector3.MoveTowards(doorL.transform.localScale, closed, 0.2f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator OpenDoorsCoroutine()
    {
        while (doorL.transform.localScale.y != 0)
        {
            doorR.transform.localScale = doorL.transform.localScale = Vector3.MoveTowards(doorL.transform.localScale, open, 0.2f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    internal bool IsEmpty()
    {
        return creatures.Count == 0;
    }
}
