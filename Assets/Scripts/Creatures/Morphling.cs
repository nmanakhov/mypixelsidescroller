﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Morphling : MonoBehaviour {
    public SpriteRenderer[] spriteRenderers;

    public virtual void Morph()
    {
        for (int i = 0; i < spriteRenderers.Length; i++)
            spriteRenderers[i].color = new Color(Random.Range(0.2f,0.8f), Random.Range(0.2f, 0.8f), Random.Range(0.2f, 0.8f), 0.7f);
    }
}
