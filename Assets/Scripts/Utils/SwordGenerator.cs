﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordGenerator : MonoBehaviour {
    public Sprite[] handles;
    public Sprite[] blades;
    int count = 0;
    public GameObject GenerateSword()
    {
        GameObject sword = new GameObject(GenerateName());
        SpriteRenderer sr = sword.AddComponent<SpriteRenderer>();
        sr.sprite = handles[Random.Range(0, handles.Length)];
        sr.sortingOrder = 6;
        sword.transform.position = new Vector3(0f, -count * 10, 0f);
        count++;
        GameObject blade = new GameObject("Blade");
        blade.transform.SetParent(sword.transform);
        blade.transform.localPosition = new Vector3(2f,0f,0f);
        sr = blade.AddComponent<SpriteRenderer>();
        sr.sortingOrder = 5;
        sr.sprite = blades[Random.Range(0, blades.Length)];
        return sword;
    }

    public void CheckGeneration()
    {
        GenerateSword();
    }
    public string GenerateName()
    {
        return "Sword";
    }
}
