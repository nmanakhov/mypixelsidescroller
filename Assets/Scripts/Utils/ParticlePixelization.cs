﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlePixelization : MonoBehaviour {
    public Sprite sprite;
    ParticleSystem.Particle[] particleArray;
    ParticleSystem particleSystem;
    public void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
      //  particleSystem.Stop();
        Pixelize(sprite);
    }

    public void Pixelize(Sprite sprite)
    {
        if (sprite == null)
            return;
        List<ParticleSystem.Particle> particles;
        particles = new List<ParticleSystem.Particle>();
        Texture2D texture = sprite.texture;
        Rect rect = sprite.textureRect;
        float size = 1;
        for (int i = 0; i < rect.width; i++)
            for (int j = 0; j < rect.height; j++)
            {
                Color c = texture.GetPixel((int)(rect.x + i), (int)(rect.y + j));
                if (c.a > 0)
                {
                    ParticleSystem.Particle particle = new ParticleSystem.Particle();
                    particle.position = new Vector3(i, j);
                    particle.startSize = size;
                    particle.startColor = c;
                    particle.velocity = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f));
                    particles.Add(particle);
                }
            }
        particleArray = particles.ToArray();
        particleSystem.SetParticles(particleArray, particleArray.Length);
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Pixelize(sprite);

        }
        // particleSystem.SetParticles(arr, arr.Length);
        RefreshParticles();
    }

    Vector3 gravity = new Vector3(0f, -2 * 9.8f,0f);
    public void RefreshParticles()
    {
        for (int i = 0; i < particleArray.Length; i++)
        {
            particleArray[i].position += particleArray[i].velocity * Time.deltaTime;
            particleArray[i].velocity += gravity *Time.deltaTime;
        }

        particleSystem.SetParticles(particleArray, particleArray.Length);
    }
}
