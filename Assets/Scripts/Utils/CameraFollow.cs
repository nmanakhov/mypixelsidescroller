﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform target;
    public Transform creature;
    Vector3 offset = new Vector3(0f, 0f, -10f);
    public float speed;
    float errorY = 0f;
    public void Update()
    {
        Vector3 newPos;
        if (creature==null)
            newPos = Vector3.Lerp(transform.position, target.position + offset, speed * Time.deltaTime);
        else
            newPos = Vector3.Lerp(transform.position, creature.transform.position + offset, speed * Time.deltaTime);
        newPos.x = Mathf.RoundToInt(newPos.x * 5) / 5f;
        if (Mathf.Abs(errorY) > 0.5f)
        {
            newPos.y += errorY;
            errorY = 0;
        }

        float delta = newPos.y;
        newPos.y = Mathf.RoundToInt(newPos.y * 5) / 5f;
        errorY += delta - newPos.y;
        transform.position = newPos;
    }
}
