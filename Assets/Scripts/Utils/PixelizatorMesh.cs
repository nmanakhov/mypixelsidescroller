﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SkinnedMeshRenderer))]
public class PixelizatorMesh : MonoBehaviour {
    List<int> triangles;
    List<Vector3> vertices;
    List<Vector2> uvs;
    Texture2D texture;
    SkinnedMeshRenderer smr;
    MeshFilter meshFilter;
    public Sprite testSprite;
    public int pixCountWidth = 1;
    public int pixCountHeight = 1;

    public void Init() {
        smr = GetComponent<SkinnedMeshRenderer>();
        if (smr == null)
           smr = gameObject.AddComponent<SkinnedMeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
            meshFilter = gameObject.AddComponent<MeshFilter>();
        smr.material = GameManager.Instance.defaultMaterial;
    }

    List<PixelMover> pixels;
    Vector3 pivot;
    public void GenerateMesh(Sprite sprite, Color color)
    {
        triangles = new List<int>();
        vertices = new List<Vector3>();
        uvs = new List<Vector2>();
        pixels = new List<PixelMover>();
        texture = sprite.texture;
        Rect rect = sprite.textureRect;
        var tempUVS = sprite.uv;
        Vector3 minUV = sprite.uv[2];
        Vector3 maxUV = sprite.uv[1];
        float pixelUVSizeX = (maxUV.x - minUV.x) / rect.width;
        float pixelUVSizeY = (maxUV.y - minUV.y) / rect.height;
        int trianglesCount = 0;
        bool ignoreAlpha = (pixCountHeight != 1 && pixCountWidth!=1);

        int rectWidth = (int)rect.width;
        int rectHeight = (int)rect.height;

        pivot = sprite.pivot;

        for (int i = 0; i < rectWidth - 1; i += pixCountWidth)
        {
            int nextI = i +pixCountWidth;
            if (nextI >= rectWidth)
                nextI = rectWidth - 1;

            for (int j = 0; j < rectHeight - 1; j += pixCountHeight)
            {
                if (!(ignoreAlpha && texture.GetPixel((int)(rect.x + i), (int)(rect.y + j)).a < 0)) {

                    int nextJ = j + pixCountHeight;
                    if (nextJ >= rectHeight)
                        nextJ = rectHeight - 1;

                    vertices.Add(new Vector3(i,                j, 0) - pivot);
                    vertices.Add(new Vector3(nextI,         j, 0) - pivot);
                    vertices.Add(new Vector3(nextI,  nextJ, 0) - pivot);
                    vertices.Add(new Vector3(i,         nextJ, 0) - pivot);
                    trianglesCount += 4;

                    triangles.Add(trianglesCount - 4);
                    triangles.Add(trianglesCount - 2);
                    triangles.Add(trianglesCount - 3);

                    triangles.Add(trianglesCount - 4);
                    triangles.Add(trianglesCount - 1);
                    triangles.Add(trianglesCount - 2);
                    Vector3 speed = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f)+40f);
                    pixels.Add(new PixelMover(speed, trianglesCount - 4));
                    uvs.Add(new Vector2(minUV.x + pixelUVSizeX * i, minUV.y + pixelUVSizeY * j));
                    uvs.Add(new Vector2(minUV.x + pixelUVSizeX * nextI, minUV.y + pixelUVSizeY * j));
                    uvs.Add(new Vector2(minUV.x + pixelUVSizeX * nextI, minUV.y + pixelUVSizeY * nextJ));
                    uvs.Add(new Vector2(minUV.x + pixelUVSizeX * i, minUV.y + pixelUVSizeY * nextJ));

                }
            }
        }
        if (smr.sharedMesh!=null)
        Destroy(smr.sharedMesh);
        Mesh mesh = new Mesh();
        smr.material.SetTexture("_MainTex", texture);
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles,0);
        mesh.SetUVs(0, uvs);
        smr.sharedMesh = mesh;
        meshFilter.mesh = mesh;
        smr.material.color = color;
    }
    public float timer = 0f;
    public void Update()
    {
        timer += Time.deltaTime;
        for (int i = pixels.Count - 1; i >= 0; i--)
            if (!pixels[i].Move(vertices))
                pixels.RemoveAt(i);
        if (timer > 5f)
            Destroy(this.gameObject);
        smr.sharedMesh.SetVertices(vertices);
        smr.sharedMesh.SetTriangles(triangles, 0);
        smr.sharedMesh.SetUVs(0, uvs);
    }

    class PixelMover {
        Vector3 speed;
        int fitstId;
        public PixelMover(Vector3 speed, int firstId)
        {
            this.speed = speed;
            this.fitstId = firstId;
        }

        public bool Move(List<Vector3> vertices)
        {
            for (int i =0; i< 4; i++)
            vertices[fitstId + i] += speed * Time.deltaTime;
            speed.y -= 5*9.8f*Time.deltaTime;
            if (vertices[fitstId].y < -30)
                return false;
            return true;
        }
    }
}
