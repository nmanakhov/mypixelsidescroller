﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpstairsUI : MonoBehaviour {
    RectTransform rTransform;
    Camera cam;

    public GameObject up;
    public GameObject down;
    public Vector3 offset;
    float timer = 0f;
    int triggerCount = 0;
    public void Start()
    {
        cam = Camera.main;
        rTransform = GetComponent<RectTransform>();       
    }


    public void ShowButtons()
    {
        triggerCount++;
        up.SetActive(KnightController.Instance.stage < KnightController.Instance.maxStage - 2);
        down.SetActive(KnightController.Instance.stage > 0);
    }

    public void HideButtons()
    {
        triggerCount--;
        if (triggerCount <= 0)
        {
            up.SetActive(false);
            down.SetActive(false);
        }
    }

    KnightController knightController;
    
    public void UpButtonClick()
    {
        if (knightController == null)
            knightController = KnightController.Instance;
        knightController.StageUp();
    }

    public void DownButtonClick()
    {
        if (knightController == null)
            knightController = KnightController.Instance;
        knightController.StageDown();
    }
}
