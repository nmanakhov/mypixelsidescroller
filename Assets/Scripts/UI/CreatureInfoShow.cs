﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureInfoShow : MonoBehaviour {
    public Creature creature;
    PlayerInputHandler inputHandler;
    [Header("UI Components")]
    public Text levelText;
    public RectTransform healthImage;
    public Text healthText;
    public RectTransform experienceImage;
    public Text experienceText;
    public Text creatureText;
    public ActionPointController actionPointController;
    public void Start()
    {
        Init();
    }

    public void Init()
    {
        if (creature == null)
            return;
        inputHandler = creature.GetComponent<PlayerInputHandler>();
        RefreshViews();
        creature.onHealthChanged += RefreshViews;
        creature.onExperienceChanged += RefreshViews;
        creature.onLevelUp += RefreshViews;
        inputHandler.onActionPointsChanged += RefreshViews;
        creatureText.text = creature.name;
    }

    Vector3 zero = Vector3.zero;
    public void RefreshViews()
    {

        if (creature == null)
            return;

        if (creature.currentHealth <= 0f)
            gameObject.SetActive(false);

        if (levelText !=null)
            levelText.text = creature.level.ToString();

        if (healthText != null)
            healthText.text = creature.currentHealth + "/" + creature.maxHealth;

        if (healthImage != null)
        {
            healthImage.anchorMax = new Vector2((float)creature.currentHealth / (float)creature.maxHealth, 1f);
            healthImage.anchoredPosition = zero;
        }

        if (experienceText!=null)
            experienceText.text = creature.experience + "/" + GameManager.experienceTable[creature.level];

        if (experienceImage != null)
        {
            experienceImage.anchorMax = new Vector2((float)creature.experience / (float)GameManager.experienceTable[creature.level], 1f);
            experienceImage.anchoredPosition = zero;
        }

        if (creatureText!=null)
            creatureText.text = creature.name;

        if (actionPointController != null)
            actionPointController.SetActionPoints(inputHandler.actionPoints);
    }

    public void RemoveEvents()
    {
        creature.onHealthChanged -= RefreshViews;
        creature.onExperienceChanged -= RefreshViews;
        creature.onLevelUp -= RefreshViews;
        inputHandler.onActionPointsChanged -= RefreshViews;
    }
}
