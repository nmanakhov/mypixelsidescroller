﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionPointController : MonoBehaviour {
    public Sprite active;
    public Sprite inactive;
    public Image[] images;
    public void SetActionPoints(int count)
    {
        for (int i = 0; i < images.Length; i++)
        {
            if (i >= count)
                images[i].sprite = inactive;
            else
                images[i].sprite = active;
        }
    }
}
