﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightSystemUI : MonoBehaviour {
    public GameObject[] buttons;
    public void SetButtons(bool flag)
    {
        foreach (var button in buttons)
            button.SetActive(flag);
    }

}
