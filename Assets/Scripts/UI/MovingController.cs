﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MovingController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    public float inputValue = 1;
    PlayerInputHandler playerInputHandler;
    public Image sr;
    public Sprite clicked;
    public Sprite normal;
    public void Start()
    {
        playerInputHandler = KnightController.Instance.GetComponent<PlayerInputHandler>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        playerInputHandler.Move(inputValue);
        sr.sprite = clicked;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        playerInputHandler.Move(0);
        sr.sprite = normal;
    }
}
