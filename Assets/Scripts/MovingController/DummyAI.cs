﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyAI : PlayerInputHandler {

    public override void Move(float input)
    {
        base.Move(input);
    }
    Transform enemy;
    public void Update()
    {
        if (enemy == null)
            enemy = KnightController.Instance.transform;

        if (IsFight && !this.creatureController.IsBusy())
        {
            if (creatureController.MoveByGrid((enemy.position - this.transform.position).x))
                ChangeActionPoint(-1);
            else {
                Debug.Log("Use skill");
                creatureController.UseSkill(0);
            }
        }
    }
}
