﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureController : MonoBehaviour {

    public CustomAnimator customAnimator;
    protected Transform creatureTransform;
    protected Vector3 targetPosition;
    public float speed = 10f;
    protected Vector3 scale;
    public delegate void CreatureState();
    protected Stack<CreatureState> currentState = new Stack<CreatureState>();
    protected Vector3 border;
    public Room currentRoom;
    [HideInInspector]
    public int stage = 0;
    public Vector3 offset = Vector3.zero;
    public List<Skill> skills;
    protected GameManager gameManager;
    protected PlayerInputHandler playerInputHandler;
    public void Start()
    {
        Init();
    }

    protected virtual void Init()
    {
        gameManager = GameManager.Instance;
        customAnimator = GetComponent<CustomAnimator>();
        playerInputHandler = GetComponent<PlayerInputHandler>();
        targetPosition = customAnimator.transform.position;
        creatureTransform = customAnimator.transform;
        scale = creatureTransform.localScale;
        currentState.Push(Stay);
    }

    public void UseSkill(int i)
    {      
        CreatureController enemy = gameManager.fightController.GetEnemy(this);
        if ((skills.Count <= i) || (!playerInputHandler.IsFight && !playerInputHandler.IsMoving) || currentState.Peek() == skills[i].Use)
            return;
        skills[i].Init(this, enemy);
        currentState.Push(skills[i].Use);
    }

    public void Update()
    {
        Refresh();
    }
    
    public virtual void Refresh()
    {
        CheckInputs();
        if (currentState.Count == 0)
            currentState.Push(Stay);
        currentState.Peek()();
    }
    [HideInInspector]
    public float input;
    public void CheckInputs()
    {
        if (input != 0f)
        {
            targetPosition.x = transform.position.x + input;
            targetPosition.y = transform.position.y;
        }
    }

    public void ChangeLookWay()
    {
        if (creatureTransform.position.x < targetPosition.x)
        {
            scale.x = Mathf.Abs(scale.x);
            creatureTransform.localScale = scale;
        }
        else {
            scale.x = -Mathf.Abs(scale.x);
            creatureTransform.localScale = scale;
        }
    }
    public LayerMask movingMask;
    public void Run()
    {
        Physics2D.queriesHitTriggers = false;
        Vector3 delta = transform.position - targetPosition;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -delta, 20, movingMask);
        if (Mathf.Abs(creatureTransform.position.x - targetPosition.x) < 1.5f || hit.collider)
        {
            currentState.Pop();
            currentState.Push(Stay);
            targetPosition = creatureTransform.position;
            return;
        }

        creatureTransform.position = Vector3.MoveTowards(creatureTransform.position, targetPosition, speed * Time.deltaTime);
        customAnimator.SetAnimation("Run");
        ChangeLookWay();
    }

    public virtual void Stay()
    {
        customAnimator.SetAnimation("Stay");
        if (Mathf.Abs(creatureTransform.position.x - targetPosition.x) > 5f)
        {
            currentState.Pop();
            currentState.Push(Run);
        }
    }

     public void AddCreatureState(CreatureState state)
    {
        currentState.Push(state);
    }

    public void SetCreatureState(CreatureState state)
    {
        while (currentState.Pop()!=null) ;
        currentState.Push(state);
    }

    public bool IsBusy()
    {
        return currentState.Peek() != Stay;
    }

    public virtual bool MoveByGrid(float input)
    {
        if (input != 0)
        {
            targetPosition.x = Mathf.RoundToInt(input / Mathf.Abs(input) + transform.position.x / 32f) * 32;
            Vector3 delta = transform.position - targetPosition;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, -delta, 20, movingMask);
            if (Mathf.Abs(targetPosition.x - transform.position.x) > 2f && !hit.collider)
                return true;
        }
        return false;
    }

    public virtual CreatureState PopState()
    {
        if (currentState.Count>0)
            return currentState.Pop();
        return null;
    }
}
