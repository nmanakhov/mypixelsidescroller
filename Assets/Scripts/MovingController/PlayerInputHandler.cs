﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputHandler : MonoBehaviour {
    
    public bool IsMoving = true;
    public bool IsFight = false;
    public CreatureController creatureController;
    public Creature creature;
    public int actionPoints = 5;
    public delegate void MethodContainer();
    public event MethodContainer onActionPointsChanged;
    public void Start()
    {
        creatureController = GetComponent<CreatureController>();
        creature = GetComponent<Creature>();
    }

    public virtual void Move(float input)
    {
        if (IsMoving)
        {
            creatureController.input = input;
        }
        else {
            if (IsFight && !creatureController.IsBusy() && creatureController.MoveByGrid(input))
                   ChangeActionPoint(-1);           
        }
    }

    public virtual bool IsDead()
    {
        return creature.currentHealth <= 0;
    }

    public virtual void ChangeActionPoint(int delta)
    {
        actionPoints += delta;
        onActionPointsChanged();
    }

    public virtual void SetActionPoint(int num)
    {
        actionPoints = num;
        onActionPointsChanged();
    }
}
