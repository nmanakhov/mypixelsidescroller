﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KnightController : CreatureController {
    private static KnightController instance_;
    public static KnightController Instance {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<KnightController>();
            return instance_;
        }
    }
    public CreatureInfoShow monsterInfo;
    public CustomAnimator knight;
    Camera main;
    RoomGenerator roomGenerator;
    [HideInInspector]
    public int maxStage = 0;
    protected override void Init()
    {
        gameManager = GameManager.Instance;
        roomGenerator = RoomGenerator.Instance;
        playerInputHandler = GetComponent<PlayerInputHandler>();
        maxStage = roomGenerator.stageNum;
        border = new Vector3(roomGenerator.stageWidth * 32f, 0f);
        main = Camera.main;
        base.Init();
    }

    public override void Refresh()
    {
        base.Refresh();

        if (this.transform.position.x < 0)
        {
            this.transform.position += border;
            targetPosition += border;
            main.transform.position += border;
        }
        if (this.transform.position.x > border.x)
        {
            this.transform.position -= border;
            targetPosition -= border;
            main.transform.position -= border;
        }
    }

      
    public void StageUp()
    {
        stage++;
        transform.position = new Vector3(transform.position.x, stage * 5f * 32f, 0);
    }

    public void StageDown()
    {
        stage--;
        transform.position = new Vector3(transform.position.x, stage * 5f * 32f, 0);
    }


    public void RandomAttack()
    {
        int r = UnityEngine.Random.Range(2, 4);
        if (r == 2)
            currentState.Push(Attack0);
        else
            currentState.Push(Attack1);
    }


    public void Attack0()
    {
        knight.SetAnimation("Attack0");

        if (knight.currentFrame == 4)
            currentState.Pop();
    }

    public void Attack1()
    {
        knight.SetAnimation("Attack1");
        if (knight.currentFrame == 3)
            currentState.Pop();
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Room")
            currentRoom = coll.GetComponent<Room>();
        if (coll.tag == "Enemy" && playerInputHandler.IsMoving)
        {
            monsterInfo.gameObject.SetActive(true);
            monsterInfo.creature = coll.GetComponent<Creature>();
            gameManager.fightSystemUI.SetButtons(true);
            monsterInfo.Init();
        }
    }

    public void OnTriggerExit2D(Collider2D coll) {
        if (coll.tag == "Enemy" && playerInputHandler.IsMoving)
        {
            monsterInfo.gameObject.SetActive(false);
            monsterInfo.RemoveEvents();
            gameManager.fightSystemUI.SetButtons(false);
            monsterInfo.creature = null;
        }
    }
}
