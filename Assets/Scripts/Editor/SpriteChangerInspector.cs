﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(SpriteChanger))]
public class SpriteChangerInspector : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SpriteChanger sc = (SpriteChanger)target;
        sc.CurrentFrame = EditorGUILayout.IntField("Current Frame ", sc.CurrentFrame);        
    }
}
