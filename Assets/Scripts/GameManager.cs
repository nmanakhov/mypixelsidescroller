﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance_;
    public static GameManager Instance {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<GameManager>();
            return instance_;
        }
    }

    public UpstairsUI upstairsUI;
    public FightSystemUI fightSystemUI;
    public Material defaultMaterial;
    public CameraFollow cameraFollow;
    public FightController fightController;
    public static int[] experienceTable = new int[] { 0, 5, 10, 15, 20, 25, 30, 35, 40, 45};
}
